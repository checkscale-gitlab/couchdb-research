require "faker"
require "pathname"
require "time"

require "pp"

require "common"
require "couchdb"

VIEWS_DIR_PATH = "./views"
ROUGHLY_ONE_YEAR_IN_SECONDS = 3600 * 24 * 365.25

def build_record
  two_years_ago = Time.now - 2 * ROUGHLY_ONE_YEAR_IN_SECONDS
  {
    animal: Faker::Creature::Animal.name,
    date: Faker::Time.between(from: two_years_ago, to: Time.now).iso8601,
  }
end

def load_views(couch)
  views = read_views_from_disk(couch)
  couch.update_design_doc(DEFAULT_DESIGN_DOC_NAME, views)
end

def read_views_from_disk(couch)
  views = {}
  Pathname(VIEWS_DIR_PATH).glob("**/*").each do |path|
    next unless path.file?

    relative_view_file_path = path.to_s.gsub(%r{^#{VIEWS_DIR_PATH}/}, "")
    view_name, _file_name = Pathname(relative_view_file_path).split
    view_component = read_view_file(path)

    view = views[view_name] || Couchdb::View.new(view_name.to_s)
    view.set(view_component.key, view_component.value)
    views[view_name] = view
  end
  views.values
end

def read_view_file(path)
  value = Pathname(path).read.strip

  relative_view_file_path = path.to_s.gsub(%r{^#{VIEWS_DIR_PATH}/}, "")
  _view_name, file_name = Pathname(relative_view_file_path).split
  key =
    case file_name.to_s
    when %r{^(.+)\.js$}
      $~[1]
    else
      file_name.to_s
    end

  Couchdb::ViewComponent.new(key, value)
end

couch = Couchdb.new(COUCHDB_BASE_URL, COUCHDB_DB_NAME)
couch.reset_database

load_views(couch)

1000.times do
  couch.create_document(build_record)
end
