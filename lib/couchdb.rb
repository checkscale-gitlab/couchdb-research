require "http"

class Couchdb
  class ViewComponent < Struct.new(:key, :value)
  end

  class View
    def initialize(name)
      @name = name
      @components = {}
    end

    attr_reader :name, :components

    def set(key, value)
      components[key] = value
    end
  end

  class NotFoundError < StandardError
    def initialize(response)
      @response = response
    end

    attr_reader :response

    def to_s
      response.status.to_s
    end
  end

  def initialize(base_url, db_name)
    @base_url = base_url
    @db_name = db_name
  end

  attr_reader :base_url, :db_name

  def reset_database
    begin
      head("")
    rescue NotFoundError
      # Nothing to do
    else
      delete("")
    end

    put("")
  end

  def create_document(doc)
    post("/", doc)
  end

  def update_design_doc(name, views)
    views_as_json = views.each_with_object({}) do |view, all|
      all[view.name] = view.components
    end
    put(design_doc_uri(name), { views: views_as_json })
  end

  def query_view(design_doc_name, view_name, query_params = {})
    response = get(view_uri(design_doc_name, view_name), query_params)
    JSON.parse(response.body.to_s)
  end

  private

  def request(method, uri, query = nil, data = nil)
    opts = {}

    if query
      opts[:params] = query
    end

    if data
      opts[:json] = data
    end

    response = Http.
      accept(:json).
      send(method, url(uri), opts)

    if response.status.to_s !~ %r{^2..\b}
      case response.status
      when 404
        raise NotFoundError.new(response)
      else
        raise response.body
      end
    end

    response
  end

  def delete(uri)
    request(:delete, uri)
  end

  def get(uri, query_params = {})
    request(:get, uri, query_params)
  end

  def head(uri)
    request(:head, uri)
  end

  def post(uri, data = nil)
    request(:post, uri, nil, data)
  end

  def put(uri, data = nil)
    request(:put, uri, nil, data)
  end

  def url(uri)
    %{#{base_url}/#{db_name}#{uri}}
  end

  def design_doc_uri(name)
    %{/_design/#{name}}
  end

  def view_uri(design_doc_name, view_name)
    %{#{design_doc_uri(design_doc_name)}/_view/#{view_name}}
  end
end
